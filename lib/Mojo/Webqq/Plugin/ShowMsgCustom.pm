package Mojo::Webqq::Plugin::ShowMsgCustom;
our $PRIORITY = 100;
use POSIX qw(strftime);
use List::Util qw(first);

# MySql数据库配置
use DBI;
use POSIX;
print "Perl MySQL Connect Demo", "\n";
# MySQL database configuration
my $host = '192.168.0.1';
my $db = "qqbot";
my $username = "root";
my $password = 'test123';
# connect to MySQL database
my %attr = ( PrintError=>0,  # turn off error reporting via warn()
             RaiseError=>1);   # turn on error reporting via die()

# Worktile集成
use LWP::UserAgent;
my $ua = LWP::UserAgent->new;
my $server_endpoint = "https://hook.worktile.com/incoming/b116d54167ca4b9cba181675a4f922e6";
# set custom HTTP request header fields
my $req = HTTP::Request->new(POST => $server_endpoint);
$req->header('content-type' => 'application/json');
$req->header('x-auth-token' => 'kfksj48sdfj4jd9d');


sub call{
    my $client = shift;
    my $data = shift;
    $client->on(
        receive_message=>sub{
            my($client,$msg)=@_;
            if($msg->type eq 'group_message'){
                my $gname = $msg->group->name;
                my $sender_nick = $msg->sender->displayname;
                return if ref $data->{ban_group}  eq "ARRAY" and first {$_=~/^\d+$/?$msg->group->uid eq $_:$gname eq $_} @{$data->{ban_group}};
                return if ref $data->{allow_group}  eq "ARRAY" and !first {$_=~/^\d+$/?$msg->group->uid eq $_:$gname eq $_} @{$data->{allow_group}};
                $client->msg({time=>$msg->time,level_color=>'cyan',level=>"群消息",title_color=>'cyan',title=>"$sender_nick|$gname :",content_color=>'cyan'},$msg->content);

                #print $msg->group->uid, "\n";
                #print $msg->group->name, "\n";
                #print $msg->time, "\n";
                #print $msg->sender->uid, "\n";
                #print $msg->sender->id, "\n";
                #print $msg->sender_id, "\n";
                #print $msg->sender->displayname, "\n";
                #print $msg->content, "\n";

                my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) = localtime($msg->time);
                my $current_time = strftime("%Y-%m-%d %H:%M:%S", $sec, $min, $hour, $wday, $mon, $year, $mday);
                #print $current_time;

                # Worktile集成
                my $qq_group_message =sprintf '[%s] [群消息] %s | %s :\n %s', $current_time, $msg->sender->displayname, $msg->group->name, $msg->content;
                #print $qq_group_message, "\n";

                worktile_biz($qq_group_message);
                mysql_biz($msg, $current_time);

            }
            elsif($msg->type eq 'sess_message'){
                my $sender_nick;
                my $receiver_nick = "我";
                my $gname;
                my $dname;
                if($msg->via eq "group"){
                    $sender_nick = $msg->sender->displayname;
                    $gname = $msg->group->name;
                    $client->msg({time=>$msg->time,level_color=>'green',level=>"群临时消息",title_color=>'green',title=>"$sender_nick|$gname :",content_color=>'green'},$msg->content);
                }
                elsif($msg->via eq "discuss"){
                    $sender_nick = $msg->sender->displayname;
                    $dname = $msg->discuss->name;
                    $client->msg({time=>$msg->time,level_color=>'green',level=>"讨论组临时消息",title_color=>'green',title=>"$sender_nick|$dname :",content_color=>'green'},$msg->content);
                }
            }
        },
        send_message=>sub{
            my($client,$msg)=@_;
            my $attach = '';
            if($msg->is_success){
                if($client->log_level eq 'debug' and defined $msg->info and $msg->info ne "发送正常" ){
                    $attach = "[" . $msg->info . "]";
                }
            }
            else{
                $attach = "[发送失败".(defined $msg->info?"(".$msg->info.")":"") . "]";
            }
            if($msg->type eq 'friend_message'){
                my $sender_nick = "我";
                my $receiver_nick = $msg->receiver->displayname;
                $client->msg({time=>$msg->time,level_color=>'green',level=>"好友消息",title_color=>'green',title=>"$sender_nick->$receiver_nick :",content_color=>'green'},$msg->content . $attach);
            }
            elsif($msg->type eq 'group_message'){
                my $gname = $msg->group->name;
                my $sender_nick = "我";
                $client->msg({time=>$msg->time,level_color=>'cyan',level=>"群消息",title_color=>'cyan',title=>"$sender_nick->$gname :",content_color=>'cyan'},$msg->content . $attach);
            }
            elsif($msg->type eq 'discuss_message'){
                my $dname = $msg->discuss->name;
                my $sender_nick = "我";
                $client->msg({time=>$msg->time,level_color=>'magenta',level=>"讨论组消息",title_color=>'magenta',title=>"$sender_nick->$dname :",content_color=>'magenta'},$msg->content . $attach);
            }
            elsif($msg->type eq 'sess_message'){
                my $sender_nick = "我";
                my $receiver_nick;
                my $gname;
                my $dname;
                if($msg->via eq "group"){
                    $receiver_nick = $msg->receiver->displayname;
                    $gname = $msg->group->name;
                    $client->msg({time=>$msg->time,level_color=>'green',level=>"群临时消息",title_color=>'green',title=>"$sender_nick->$receiver_nick|$gname :",content_color=>'green'},$msg->content . $attach);
                }
                elsif($msg->via eq "discuss"){
                    $receiver_nick = $msg->receiver->displayname;
                    $dname = $msg->discuss->name;
                    $client->msg({time=>$msg->time,level_color=>'green',level=>"讨论组临时消息",title_color=>'green',title=>"$sender_nick->$receiver_nick|$dname :",content_color=>'green'},$msg->content . $attach);
                }
            }
        }
    );
}


sub mysql_biz {
    my $msg = shift;
    my $current_time = shift;

    # MySql连接
    my $dbh  = DBI->connect("DBI:mysql:$db;host=$host",$username,$password, \%attr);
    #print "Connected to the MySQL database.", "\n";
    my $sql = "INSERT INTO qq_group_msg(group_id,group_name,msg_time,sender_id,sender_name,msg_content) VALUES(?,?,?,?,?,?)";
    my $stmt = $dbh->prepare($sql);
    $stmt->execute($msg->group-uid,$msg->group->name,$current_time,$msg->sender->uid,$msg->sender->displayname,$msg->content);

    $stmt->finish();
    $dbh->disconnect();
}

sub worktile_biz {
    my $qq_group_message = shift;

    #my $post_data = '{"text": "'.$qq_group_message.'"}';
    my $post_data = sprintf '{"text": "%s"}', $qq_group_message;
    $post_data =~ s/[\n\r]/\\n/g; #转义回车换行
    #print $post_data, "\n";
    $req->content($post_data);

    my $resp = $ua->request($req);
    if ($resp->is_success) {
        my $message = $resp->decoded_content;
        print "Received reply: $message\n";
    }
    else {
        print "HTTP POST error code: ", $resp->code, "\n";
        print "HTTP POST error message: ", $resp->message, "\n";
    }

}

1;

use 5.010;
use strict;
use warnings;

use DBI;
use POSIX;
say "Perl MySQL Connect Demo";
# MySQL database configuration
my $host = '192.168.0.1';
my $db = "qqbot";
my $username = "root";
my $password = 'test123';
# connect to MySQL database
my %attr = ( PrintError=>0,  # turn off error reporting via warn()
             RaiseError=>1);   # turn on error reporting via die()

# MySql连接
my $dbh  = DBI->connect("DBI:mysql:$db;host=$host",$username,$password, \%attr);
print "Connected to the MySQL database.", "\n";
my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) = localtime(time);
my $current_time = strftime("%Y-%m-%d %H:%M:%S", $sec, $min, $hour, $wday, $mon, $year, $mday);
print $current_time;
my $sql = "INSERT INTO qq_group_msg(group_id,group_name,msg_time,sender_id,sender_name,msg_content) VALUES('001','test001','$current_time','01','test01','hello01');";

my $stmt = $dbh->prepare($sql);
$stmt->execute();

$stmt->finish();
$dbh->disconnect();


FROM perl:5.20
MAINTAINER crane-yuan <1182248598@qq.com>
COPY . /usr/src/qq-bot
WORKDIR /usr/src/qq-bot
ENV TZ=Asia/Shanghai
#RUN apt-get update && apt-get install -y libdbd-mysql-perl
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN cpanm Encode::Locale IO::Socket::SSL Mojolicious DBI POSIX LWP::UserAgent LWP::Protocol::https MIME::Base64 MIME::Lite Mojo::SMTP::Client DBD::mysql 
#RUN git clone https://crane-yuan@bitbucket.org/crane-yuan/qq-bot.git \
#    && cd Mojo-Webqq-master \
#    && cd qq-bot \
#CMD nohup perl qq-service-custom.pl > qqbot.log 2>&1 &
CMD perl qq-service-custom.pl
